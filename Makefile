CC=gcc
IDIR=./includes
VAR1=point.h
CFLAGS=-c -I$(IDIR) -lm
all: Distancia

Distancia: main.o point.o ./includes/$(VAR1)
	$(CC) -o Distancia main.c point.c -I$(IDIR)  -lm

main.o: main.c
	$(CC) $(CFLAGS) main.c

point.o: point.c
	$(CC) $(CFLAGS) point.c

clean:
	rm *o Distancia 

