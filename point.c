#include <point.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define SIZE 3

static int Point1[SIZE];
static int Point2[SIZE];
static double resp;
/*
void solonumeros(){
	float num,ch,ok;
	do{
		printf("ingresa un numero: ");
		fflush(stdout);
		if((ok=scanf("%f",&num))==EOF)
			return EXIT_FAILURE;

		while((ch=getchar())!=EOF && ch!='\n')
	}
	while(!ok);
	return EXIT_SUCCESS;
}

*/
typedef struct Point{
	float x;
	float y;
	float z;
}Point;

Point punto_medio(Point p1,Point p2){
        Point pmedio;
        pmedio.x=((p1.x+p2.x)/2);
        pmedio.y=((p1.y+p2.y)/2);
        pmedio.z=((p1.z+p2.z)/2);
return pmedio;
}

void distancia2puntos(void){
	Point p1,p2,Puntomedio;
	printf("Bienvenidos al programa que calcula la distancia entre 2 puntos en el espacio\n");
	printf("Ingrese las coordenadas para el primer punto(X,Y,Z):\n");
	scanf("%f,%f,%f",&p1.x,&p1.y,&p1.z);

	printf("Ingrese las coordenadas para el segundo punto(X,Y,Z):\n");
	scanf("%f,%f,%f",&p2.x,&p2.y,&p2.z);
	resp=sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2)+pow((p1.z-p2.z),2));	
	printf("La distancia es: %.2f\n",resp);

	Puntomedio=punto_medio(p1,p2);
	printf("\nY el punto medio es: (%.2f,%.2f,%.2f)\n", Puntomedio.x,Puntomedio.y,Puntomedio.z);
}
